package modelo;

import java.io.*;

//Importamos todas las clases de java.io.
public class FicheroTextoApp {
	public static void main(String[] args) {
		try (FileWriter fw = new FileWriter("C:/file/fichero1.txt");
				FileReader fr = new FileReader("C:/file/fichero1.txt")) {
			escribeFichero(fw);
			// Guardamos los cambios del fichero
			fw.flush();
			fw.close();
			leeFichero(fr);
			fr.close();
		} catch (IOException e) {
			System.out.println("Error E/S: " + e);
		}
	}

	public static void escribeFichero(FileWriter fw) throws IOException {
		// Escribimos en el fichero
		fw.write("Esto es una prueba");

	}

	public static void leeFichero(FileReader fr) throws IOException {
		// Leemos el fichero y lo mostramos por pantalla
		int valor = fr.read();
		while (valor != -1) {
			System.out.print((char) valor);
			valor = fr.read();
		}
	}
}